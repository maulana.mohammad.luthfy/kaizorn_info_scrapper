require('dotenv').config()
const puppeteer = require('puppeteer')
const axios = require('axios').default
// const minimist = require('minimist')
const nodemailer = require('nodemailer')
const { ShareServiceClient, StorageSharedKeyCredential } = require("@azure/storage-file-share")
const moment = require('moment-timezone')
const iconv = require('iconv-lite')
const express = require('express')
const app = express()
const fs = require('fs')

moment.locale('de')
// moment.tz.add('Europe/Berlin Op0')

app.get('/scrap-news', async function (req, res) {
  const { link, type, location } = req.query

  const browser = await puppeteer.launch({ headless: true })
  const page = await browser.newPage()
  await page.goto(process.env.NWSPOOL_URL)
  await page.type('#modlgn-username', process.env.NWSPOOL_USRNM)
  await page.type('#modlgn-passwd', process.env.NWSPOOL_PSSWD)
  await page.click('input[type="submit"]')
  await page.waitForSelector('a[href="/index.php/wettertexte-blr-radiodienst"]', { visible: true })

  await page.click('a[href="/index.php/wettertexte-blr-radiodienst"]')
  await page.waitForSelector('#adminForm table.category')
  await page.click(`a[href="/index.php/wettertexte-blr-radiodienst/${link}"]`)
  await page.waitForSelector('.item-page')

  let data = await page.$eval('.item-page', () => {
    let title = 'BLR Wetter-Redaktion: ' + document.querySelector('title').innerHTML.substr(5).trim()

    let dateTitle = title.split(',')

    let rawContent = Array.from(document.querySelector('.item-page').children).filter(dom => !['ul', 'div'].includes(dom.localName))
    let contentHtmlFormat = rawContent.map(dom => dom.outerHTML).join('')
    let contentTextFormat = [...['BLR Wetter CMS Gateway', title, ''], ...rawContent.map(dom => dom.innerText).map(t => t.trim())].join("\n")

    // return { content, headline: title, subcategories: '["Wetter"]', creator: 'kaizorn.info' }
    return { titleHour: dateTitle[2] || null, titleDate: dateTitle[1], urgency: 3, date: null, textFormat: contentTextFormat, content: `<h1>${title.replace('BLR Wetter-Redaktion: ', '')}</h1>${contentHtmlFormat}`, headline: title, subcategories: ['wetter'], creator: 'kaizorn.info', location: null, token: null, category: 'blr' }
  })

  // let tmpDate = data.headline.split(',').slice(1).map(t => t.replace('h', '')).map(t => t.trim()).join(' ')
  // let tmpDate = data.headline.split(',').slice(1).map(t => t.replace('h', '')).map(t => t.trim()).join(' ')
  // if (!tmpDate.includes(':')) tmpDate += moment.tz(moment().format('YYYY-MM-DD HH:mm', 'Europe/Berlin')).format('HH:mm') // special case for Bio and Alpen append hour manually
  // data.date = moment(tmpDate, 'D. MMM YYYY HH:mm').format('YYYY-MM-DD HH:mm')
  if (data.titleHour) {
    data.titleHour = data.titleHour.replace('/h|o\'clock|am|pm/ig', '').trim()

    if (data.titleHour.length <= 2) data.titleHour = `${data.titleHour.padStart(2, '0')}:00`
  }
  let momentDateTitle = moment(`${data.titleDate} ${data.titleHour || moment.tz(moment(), 'Europe/Berlin').format('HH:mm')}`, 'DD. MMMM YYYY HH:mm')
  data.dateTitle = moment(`${data.titleDate} ${data.titleHour || moment.tz(moment(), 'Europe/Berlin').format('HH:mm')}`, 'DD. MMMM YYYY HH:mm').format('YYYY-M-D HH:mm')
  data.date = moment.tz(moment(), 'Europe/Berlin').format('YYYY-MM-DD HH:mm')
  data.subcategories.push(type)
  data.location = location
  data.token = process.env.NWSPOOL_URL_PUSH_TOKEN

  let diffHours = moment.tz(moment(), 'Europe/Berlin').diff(momentDateTitle, 'hours') //momentDateTitle.diff(moment(), 'hours')
  const params = new URLSearchParams()

  Object.keys(data).forEach(key => {
    if (Array.isArray(data[key])) {
      data[key].forEach(value => {
        params.append(`${key}[]`, value)
      })
    } else {
      params.append(key, data[key])
    }
  })

  const config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  }

  let response = {}
  let statusCode = 200
  let payload = {}
  try {
    if (diffHours < 2) { // NWSPL-185
      response = await axios.post(process.env.NWSPOOL_URL_PUSH, params, config)
      let rule = new RegExp('success')

      if (rule.test(response.data.message)) {
        // send email
        let transporter = nodemailer.createTransport({
          host: process.env.MAIL_SMTP_SERVER,
          port: process.env.MAIL_SMTP_PORT,
          secure: false, // true for 465, false for other ports
          auth: {
            user: process.env.MAIL_NAME, // generated ethereal user
            pass: process.env.MAIL_PASSWORD, // generated ethereal password
          },
        });

        let mailOptions = {
          to: process.env.MAIL_TARGET,
          from: `BLR Wetter-Redaktion ${process.env.MAIL_NAME}`,
          cc: process.env.MAIL_CC,
          bcc: process.env.MAIL_BCC,
          subject: data.headline,
          html: data.content,
        };

        let info = await transporter.sendMail(mailOptions);

        await browser.close()

        // send file
        const account = process.env.AZURE_ACCOUNT
        const accountKey = process.env.AZURE_KEY

        const credential = new StorageSharedKeyCredential(account, accountKey)
        const serviceClient = new ShareServiceClient(
          `https://${account}.file.core.windows.net`,
          credential
        )

        const shareName = process.env.AZURE_FILESHARE_NAME
        const directoryName = process.env.AZURE_FILESHARE_TARGET_FOLDER

        const directoryClient = serviceClient.getShareClient(shareName).getDirectoryClient(directoryName);

        let timestamp = moment.tz(moment.now(), "Europe/Berlin").format('DD-MM-yyyy hh.mm')
        const content = iconv.encode(data.textFormat, 'CP850')
        const fileName = `${type} ${timestamp}.txt`
        const fileClient = directoryClient.getFileClient(fileName);
        await fileClient.create(content.length);
        // console.log(`Create file ${fileName} successfully`);

        // Upload file range
        let status = {}
        try {
          status = await fileClient.uploadRange(content, 0, content.length)
        } catch (error) {
          return res.status(400).json({ 'message': error })
        }

        payload = { news: response.data, email: info.id, upload: status }
      } else {
        statusCode = 400
        payload = { news: response.data }
      }

      fs.appendFile('logs/' + moment().format('YYYY-MM-DD') + '.log', `${data.headline} - ${data.date}`, function (err) {
        if (err) throw err;
        console.log('Saved!');
      });
    }
  } catch (error) {
    return res.status(400).json([{ 'message': 'failed to insert news' }])
  }

  return res.status(statusCode).json(payload)
})

app.listen(3000)