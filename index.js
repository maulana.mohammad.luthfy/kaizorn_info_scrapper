require('dotenv').config()
const puppeteer = require('puppeteer')
const axios = require('axios').default
const minimist = require('minimist')
const nodemailer = require('nodemailer')
const { ShareServiceClient, StorageSharedKeyCredential } = require("@azure/storage-file-share")
const moment = require('moment')
const iconv = require('iconv-lite')

let args = minimist(process.argv.slice(2), {
    alias: {
        i: 'link',
        t: 'type',
        o: 'location'
    }
})

try {
  (async () => {
    const browser = await puppeteer.launch({headless: true})
    const page = await browser.newPage()
    const pushUrl = process.env.NWSPOOL_URL_PUSH
    await page.goto(process.env.NWSPOOL_URL)
    await page.type('#modlgn-username', process.env.NWSPOOL_USRNM)
    await page.type('#modlgn-passwd', process.env.NWSPOOL_PSSWD)
    await page.click('input[type="submit"]')
    await page.waitForSelector('a[href="/index.php/wettertexte-blr-radiodienst"]', {visible: true})

    await page.click('a[href="/index.php/wettertexte-blr-radiodienst"]')
    await page.waitForSelector('#adminForm table.category')
    await page.click(`a[href="/index.php/wettertexte-blr-radiodienst/${args.link}"]`)
    await page.waitForSelector('.item-page')

    let data = await page.$eval('.item-page', () => {
      let title = document.querySelector('title').innerHTML

      let rawContent = Array.from(document.querySelector('.item-page').children).filter(dom => !['ul', 'div'].includes(dom.localName))
      let contentHtmlFormat = rawContent.map(dom => dom.outerHTML).join('')
      let contentTextFormat = rawContent.map(dom => dom.innerText).map(t => t.trim()).join("\n")

      // return { content, headline: title, subcategories: '["Wetter"]', creator: 'kaizorn.info' }
      return { textFormat: contentTextFormat, content: `<h1>${title}</h1>${contentHtmlFormat}`, headline: title, subcategories: ['wetter'], creator: 'kaizorn.info', location: null, token: null }
    })

  try {
    data.subcategories.push(args.type)
    data.location = args.location
    data.token = process.env.NWSPOOL_URL_PUSH_TOKEN

    const params = new URLSearchParams()

    Object.keys(data).forEach(key => {
      if (Array.isArray(data[key])) {
        data[key].forEach(value => {
          params.append(`${key}[]`, value)
        })
      } else {
        params.append(key, data[key])
      }
    })

    const config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }

    let response = await axios.post(process.env.NWSPOOL_URL_PUSH, params, config)

    console.log(response.data)


    // send email
    let transporter = nodemailer.createTransport({
      host: process.env.MAIL_SMTP_SERVER,
      port: process.env.MAIL_SMTP_PORT,
      secure: false, // true for 465, false for other ports
      auth: {
        user: process.env.MAIL_NAME, // generated ethereal user
        pass: process.env.MAIL_PASSWORD, // generated ethereal password
      },
    });
    
    let mailOptions = {
        to: process.env.MAIL_TARGET,
        from: process.env.MAIL_NAME,
        subject: data.headline,
        html: data.content,
    };
    
    let info = await transporter.sendMail(mailOptions);

    // console.log(info.messageId)
  } catch (error) {
    console.log(error.data)
  }

      // send file
      const account = process.env.AZURE_ACCOUNT
      const accountKey = process.env.AZURE_KEY
  
      const credential = new StorageSharedKeyCredential(account, accountKey)
      const serviceClient = new ShareServiceClient(
        `https://${account}.file.core.windows.net`,
        credential
      )
  
      const shareName = process.env.AZURE_FILESHARE_NAME
      const directoryName = process.env.AZURE_FILESHARE_TARGET_FOLDER
  
      const directoryClient = serviceClient.getShareClient(shareName).getDirectoryClient(directoryName);
  
      let timestamp = moment().format('DD-MM-yyyy hh.mm')
      const content = iconv.encode(data.textFormat, 'CP850')
      const fileName = `${args.type} ${timestamp}.txt`
      const fileClient = directoryClient.getFileClient(fileName);
      await fileClient.create(content.length);
      console.log(`Create file ${fileName} successfully`);
  
      // Upload file range
      await fileClient.uploadRange(content, 0, content.length)
      console.log(`Upload file ${fileName} successfully`);
  
      await page.close()
      process.exit()
  })();
} catch (error) {
  console.log(error)
}